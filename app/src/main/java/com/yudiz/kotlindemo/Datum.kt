package com.yudiz.kotlindemo


class Datum {

    var iAppId: String? = null
    var vAppName: String? = null
    var vIcon: String? = null
    var vItunes: String? = null
    var vPlaystore: String? = null
    var vDescription: String? = null
    var vCompany: String? = null
    var ePaidStaus: String? = null
    var vCategory: String? = null
    var dCreatedDate: String? = null
    var vScreenshot: List<String>? = null

}
