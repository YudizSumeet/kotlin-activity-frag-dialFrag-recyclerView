package com.yudiz.kotlindemo

class Model {

    var status: Int? = null
    var message: String? = null
    var data: List<Datum>? = null

}
