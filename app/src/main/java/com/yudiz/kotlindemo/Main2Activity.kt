package com.yudiz.kotlindemo

import android.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.ArrayList

class Main2Activity : DialogFragment() {
    private var text: TextView? = null
    private var array = ArrayList<String>()
    private var rv: RecyclerView? = null
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view: View? = inflater?.inflate(R.layout.activity_main2, container, false)
        text = view!!.findViewById(R.id.tv2) as TextView

        rv = view?.findViewById(R.id.rv) as RecyclerView
        array.add("abc")
        array.add("def")
        array.add("ghi")
        array.add("jkl")
        array.add("mno")
        rv?.layoutManager = LinearLayoutManager(activity)
        rv?.adapter = MyAdapter(array)

//        (MainActivity()).setText(2,"def")
        text!!.setOnClickListener { dismiss() }
        text?.setOnClickListener { Main2Activity().show(fragmentManager, "abc") }

        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme)
    }
}
