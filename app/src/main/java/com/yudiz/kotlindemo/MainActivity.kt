package com.yudiz.kotlindemo

import android.app.Fragment
import android.app.FragmentManager
import android.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.system.Os.bind
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private var bt: TextView? = null
    private var a = "abc"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bt = findViewById(R.id.tv) as TextView
        a = "def"

        setText(number = 1, message = "abc")

        bt?.setOnClickListener() { view -> Main2Activity().show(fragmentManager, "abc") }


        setText(message = "abc");           //named parameter

        fragmentManager.beginTransaction()?.replace(R.id.frame, Fragment1() as Fragment)?.addToBackStack("abc")?.commit()           //chaining


    }

    fun setText(message: String, number: Int = 3) {           //default parameter
        Log.i("tag", "$message + ${number + 3}")
    }

    internal fun animal(abc: String) {
        var abc = abc
        abc = "hello"

    }

}
